<?php

namespace App\Entity;

use App\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PostRepository::class)]
class Post
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'text')]
    private $content;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'id_user')]
    private $user_id;

    #[ORM\OneToMany(mappedBy: 'post_id', targetEntity: Comment::class)]
    private $post_id;

    #[ORM\OneToMany(mappedBy: 'post_id', targetEntity: Vote::class)]
    private $postVote_id;

    public function __construct()
    {
        $this->post_id = new ArrayCollection();
        $this->postVote_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getPostId(): Collection
    {
        return $this->post_id;
    }

    public function addPostId(Comment $postId): self
    {
        if (!$this->post_id->contains($postId)) {
            $this->post_id[] = $postId;
            $postId->setPostId($this);
        }

        return $this;
    }

    public function removePostId(Comment $postId): self
    {
        if ($this->post_id->removeElement($postId)) {
            // set the owning side to null (unless already changed)
            if ($postId->getPostId() === $this) {
                $postId->setPostId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Vote>
     */
    public function getPostVoteId(): Collection
    {
        return $this->postVote_id;
    }

    public function addPostVoteId(Vote $postVoteId): self
    {
        if (!$this->postVote_id->contains($postVoteId)) {
            $this->postVote_id[] = $postVoteId;
            $postVoteId->setPostId($this);
        }

        return $this;
    }

    public function removePostVoteId(Vote $postVoteId): self
    {
        if ($this->postVote_id->removeElement($postVoteId)) {
            // set the owning side to null (unless already changed)
            if ($postVoteId->getPostId() === $this) {
                $postVoteId->setPostId(null);
            }
        }

        return $this;
    }
}
