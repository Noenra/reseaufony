<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
class Comment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'user_id')]
    #[ORM\JoinColumn(nullable: false)]
    private $user_id;

    #[ORM\ManyToOne(targetEntity: Post::class, inversedBy: 'post_id')]
    #[ORM\JoinColumn(nullable: false)]
    private $post_id;

    #[ORM\OneToMany(mappedBy: 'uservote_id', targetEntity: User::class)]
    private $users;

    #[ORM\OneToMany(mappedBy: 'comment_id', targetEntity: Vote::class)]
    private $comment_id;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $nbVote;
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->comment_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getPostId(): ?Post
    {
        return $this->post_id;
    }

    public function setPostId(?Post $post_id): self
    {
        $this->post_id = $post_id;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @return Collection<int, Vote>
     */
    public function getCommentId(): Collection
    {
        return $this->comment_id;
    }

    public function addCommentId(Vote $commentId): self
    {
        if (!$this->comment_id->contains($commentId)) {
            $this->comment_id[] = $commentId;
            $commentId->setCommentId($this);
        }

        return $this;
    }

    public function removeCommentId(Vote $commentId): self
    {
        if ($this->comment_id->removeElement($commentId)) {
            // set the owning side to null (unless already changed)
            if ($commentId->getCommentId() === $this) {
                $commentId->setCommentId(null);
            }
        }
        return $this;
    }

    public function getNbVote(): ?int
    {
        return $this->nbVote;
    }

    public function setNbVote(?int $nbVote): self
    {
        $this->nbVote = $nbVote;

        return $this;
    }
}
