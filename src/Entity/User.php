<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $username;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\OneToMany(mappedBy: 'user_id', targetEntity: Post::class)]
    private $id_user;

    #[ORM\OneToMany(mappedBy: 'user_id', targetEntity: Comment::class)]
    private $user_id;

    #[ORM\Column(type: 'string', length: 255)]
    private $email;

<<<<<<< HEAD
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $image;
=======
    #[ORM\ManyToOne(targetEntity: Comment::class, inversedBy: 'users')]
    private $uservote_id;

    #[ORM\OneToMany(mappedBy: 'user_id', targetEntity: Vote::class)]
    private $userVote_id;
>>>>>>> ed0ed1beab401f95894a2fb077fec58ef25a8a86

    public function __construct()
    {
        $this->id_user = new ArrayCollection();
        $this->user_id = new ArrayCollection();
        $this->userVote_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Post>
     */
    public function getIdUser(): Collection
    {
        return $this->id_user;
    }

    public function addIdUser(Post $idUser): self
    {
        if (!$this->id_user->contains($idUser)) {
            $this->id_user[] = $idUser;
            $idUser->setUserId($this);
        }

        return $this;
    }

    public function removeIdUser(Post $idUser): self
    {
        if ($this->id_user->removeElement($idUser)) {
            // set the owning side to null (unless already changed)
            if ($idUser->getUserId() === $this) {
                $idUser->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getUserId(): Collection
    {
        return $this->user_id;
    }

    public function addUserId(Comment $userId): self
    {
        if (!$this->user_id->contains($userId)) {
            $this->user_id[] = $userId;
            $userId->setUserId($this);
        }

        return $this;
    }

    public function removeUserId(Comment $userId): self
    {
        if ($this->user_id->removeElement($userId)) {
            // set the owning side to null (unless already changed)
            if ($userId->getUserId() === $this) {
                $userId->setUserId(null);
            }
        }

        return $this;
    }

<<<<<<< HEAD
    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;
=======
    /**
     * @return Collection<int, Vote>
     */
    public function getUserVoteId(): Collection
    {
        return $this->userVote_id;
    }

    public function addUserVoteId(Vote $userVoteId): self
    {
        if (!$this->userVote_id->contains($userVoteId)) {
            $this->userVote_id[] = $userVoteId;
            $userVoteId->setUserId($this);
        }
>>>>>>> ed0ed1beab401f95894a2fb077fec58ef25a8a86

        return $this;
    }

<<<<<<< HEAD
=======
    public function removeUserVoteId(Vote $userVoteId): self
    {
        if ($this->userVote_id->removeElement($userVoteId)) {
            // set the owning side to null (unless already changed)
            if ($userVoteId->getUserId() === $this) {
                $userVoteId->setUserId(null);
            }
        }

        return $this;
    }

    
>>>>>>> ed0ed1beab401f95894a2fb077fec58ef25a8a86
    
}
