<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;


class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(ManagerRegistry $doctrine, Request $request): Response
    {
        $post = $doctrine->getRepository(Post::class)->findAll();
        $request = Request::createFromGlobals();
        $user = $this->getUser();
        
        if($request -> request -> get('title')){
            $entityManager = $doctrine->getManager();
            $user = $this->getUser();
            $newPost = new Post();
            $newPost->setUserId($doctrine->getRepository(User::class)->findOneBy(['id' => $user->getId()]));
            $newPost->setTitle($request->request->get('title'));
            $newPost->setContent($request->request->get('content'));
            $entityManager->persist($newPost);
            $entityManager->flush();
        }
        return $this->render('index/index.html.twig', [
            'posts' => $post,
        ]);
    }
}
