<?php

namespace App\Controller;


use App\Entity\Post;
use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Vote;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;


class PostController extends AbstractController
{
    #[Route('post/{id}', name: 'app_postId')]
    public function index(ManagerRegistry $doctrine, Request $request, Int $id): Response
    {
        $request = Request::createFromGlobals();
        $post = $doctrine->getRepository(Post::class)->findBy([ 'id' => $id]);
        $comment= $doctrine->getRepository(Comment::class)->findBy(['post_id' => $id]);
        $user = $this->getUser();
        $vote = $doctrine->getRepository(Vote::class)->findBy(['post_id' => $id]);
        if($request->request->get('content')){
            $entityManager = $doctrine->getManager();
            $newComment = new Comment();
            $newComment->setTitle($request->request->get('content'));
            $newComment->setPostId($doctrine->getRepository(Post::class)->findOneBy(['id' => $id]));
            $newComment->setUserId($doctrine->getRepository(User::class)->findOneBy(['id' => $user]));
            $newComment->setNbVote(0);
            $entityManager->persist($newComment);
            $entityManager->flush();
        }
        return $this->render('post/index.html.twig', [
            'posts' => $post,
            'comments' => $comment,
            'id' => $id,
        ]);
    }
    #[Route('post/{id}/vote/{ID}', name: 'app_vote')]
    public function voter(ManagerRegistry $doctrine, Int $id, Int $ID)
    {
        $entityManager = $doctrine->getManager();

        $newVote = new Vote();
        $newVote->setUserId($this->getUser());
        $newVote->setPostId($doctrine->getRepository(Post::class)->findOneBy(['id' => $id]));
        $newVote->setCommentId($doctrine->getRepository(Comment::class)->findOneBy(['id' => $ID]));
        $reputation = $doctrine->getRepository(Comment::class)->findOneBy(['id' => $ID]);
        $entityManager->persist($newVote);
        $entityManager->flush();
        // dd($reputation);
        $reputation->setNbVote($reputation->getNbVote()+ 1);
        return new RedirectResponse('/');
    }
}
