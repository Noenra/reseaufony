<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;


class ProfileController extends AbstractController
{
    #[Route('/profile', name: 'app_profile')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $request = Request::createFromGlobals();
        $user = $this->getUser();
        $post = $doctrine->getRepository(Post::class)->findBy(['user_id' => $user -> getId()]);

        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
            'posts' => $post,
        ]);
    }
}
